package steps;

import io.cucumber.java.bg.И;
import org.junit.Assert;

import static base.BaseWebElement.getField;
import static com.codeborne.selenide.Condition.visible;

public class ElementSteps {

    @И("^поле \"(.+)\" заполняется значением \"(.+)\"$")
    public void fillField(String elementName, String value) {
        getField(elementName).setValue(value);
    }

    @И("^производится нажатие на элемент \"(.+)\"$")
    public void clickElement(String elementName) {
        getField(elementName).click();
    }

    @И("^проверяется видимость элемента \"(.+)\"$")
    public void checkElement(String elementName) {
        getField(elementName).shouldBe(visible);
    }

    @И("^проверяется что стоит чекбокс \"(.+)\"$")
    public void checkCheckbox(String elementName) {
        String checked = getField(elementName).getAttribute("data-test-value");
        Assert.assertEquals("Чекбокс у элемента - " + elementName + " не стоит!", "checked", checked);
    }

    @И("^навести на элемента \"(.+)\" и нажать на элемент$")
    public void scrollTo(String elementName) {
        getField(elementName).scrollTo().click();
    }

}